pub mod run_all;
pub mod test_urls;
pub mod tmp;

pub type Result = core::result::Result<(), Box<dyn std::error::Error>>;
