use std::{path::Path, process::Command};

use crate::utils::{fs, colors};

pub fn run(sitemap_urls: &Vec<String>) -> super::Result {
    let my_rp = std::env::current_exe()?;

    let tests = fs::get_files("tests")?;
    let mut passed = Vec::new();
    let mut failed = Vec::new();

    for test in &tests {
        let test_path = Path::new("tests").join(test).canonicalize()?;
        if test_path == my_rp {
            continue;
        }

        if sitemap_urls.len() > 0 { // see TODO below
            todo!()
        }

        if Command::new(test_path)
            .env("SITEMAP_URLS", sitemap_urls.join(",")) // TODO: needs to be tested/verified that this is how multiple env values are passed
            .output()?
            .status
            .success()
        {
            passed.push(test);
            println!("{test}: {GREEN}PASS{RESET}", test = test.display(), GREEN = colors::fg::GREEN, RESET = colors::effects::RESET);
        } else {
            failed.push(test);
            println!("{test}: {RED}FAIL{RESET}", test = test.display(), RED = colors::fg::RED, RESET = colors::effects::RESET);
        }
    }

    let total = passed.len() + failed.len();
    let percentage = (passed.len() as f64 / total as f64 * 100.).to_string();
    // let percentage = percentage.strip_suffix(".0").unwrap_or(&percentage);

    println!("successful tests: {}/{total} ({percentage}%)", passed.len());

    Ok(())
}
