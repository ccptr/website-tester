use std::path::Path;

use crate::utils::{net, sitemap};

pub fn run(sitemap_urls: &Vec<String>) -> super::Result {
    let sitemap_xml_file = Path::new("/tmp/website-sitemap-sitemap.xml");

    let contents = if sitemap_xml_file.is_file() {
        [std::fs::read_to_string(sitemap_xml_file)?].to_vec()
    } else {
        let mut content = Vec::new();
        for sm in sitemap_urls {
            content.push(net::fetch_content(&sm));
        }
        content
    };

    for content in contents {
        let sitemaps = sitemap::parse_sitemap_recursive(&content)?;

        for sitemap in sitemaps {
            println!("{}", sitemap);
        }
    }

    Ok(())
}
