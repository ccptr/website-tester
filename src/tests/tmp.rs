use crate::{
    args::Args,
    utils::{net, sitemap},
};

pub fn run(args: &Args) -> super::Result {
    for sitemap_url in sitemap::fetch_sitemaps(&args.base_url)? {
        println!("{}", sitemap_url);
        let content = net::fetch_content(&sitemap_url);
        let locs = sitemap::parse_sitemap_recursive(&content)?;
        for loc in locs {
            println!("{}", loc);
        }
    }

    Ok(())
}
