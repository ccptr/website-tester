use std::{
    env,
    path::Path,
};

use structopt::StructOpt;

#[derive(StructOpt)]
pub struct Args {
    #[structopt(
        default_value = "",
        env = "HUGO_BASEURL",
        help = "hostname (and path) to the root, e.g. https://example.com/",
        long = "baseURL",
        short
    )]
    pub base_url: String,
    #[structopt(subcommand)] // Note that we mark a field as a subcommand
    pub command: Command,
}

#[derive(StructOpt)]
#[structopt(about = "multicall binary for website tests")]
pub enum Command {
    RunAll {
        #[structopt(
            default_value = "",
            long = "sitemapURL",
            env = "SITEMAP_URLS",
            help = "sitemap URL to be used instead of baseURL + /sitemap.xml"
        )]
        sitemap_urls: Vec<String>,
    },
    TestUrls {
        #[structopt(
            default_value = "",
            long = "sitemapURL",
            env = "SITEMAP_URLS",
            help = "sitemap URL to be used instead of baseURL + /sitemap.xml"
        )]
        sitemap_urls: Vec<String>,
    },
    Tmp,
}

impl Args {
    pub fn parse() -> Self {
        let mut args = Self::from_args();

        if args.base_url.len() == 0 {
            if Path::new(".hugo_build.lock").is_file() {
                args.base_url = "http://localhost:1313".to_string();
            } else {
                args.base_url = match env::var("BASEURL_FALLBACK") {
                    Ok(val) => val,
                    Err(_) => {
                        panic!("Could not determine --baseURL; try running Hugo, specifying --baseURL on the command line, or setting the \"BASEURL_FALLBACK\" environment variable.");
                    }
                };
            }
        }

        if let Command::TestUrls { sitemap_urls } = &args.command {
            if sitemap_urls.len() == 0 {
                args.command = Command::TestUrls {
                    sitemap_urls: [[&args.base_url, "/sitemap.xml"].concat()].to_vec(),
                };
            }
        }

        return args;
    }
}
