pub fn fetch_content(path: &str) -> String {
    let result = ureq::get(path).call();
    match result {
        Ok(response) => return response.into_string().unwrap(),
        Err(error) => {
            log::error!("failed to fetch web page content for {path}");
            panic!("{}", error);
        }
    }
}
