/// ##########################
/// WARNING: this is not (thoroughly) tested
/// the most testing it gets is a simple sitemap
/// ##########################
use std::{fs::File, io::Write};

use scraper::{Html, Selector};

use crate::utils::{crypto, net, robots::Robots};

// ###################
// You can find the article about this gist here:
// https://primates.dev/find-all-urls-of-a-website-in-a-few-seconds-python/
// ###################

/// usage: fetch_sitemaps('https://example.com')
/// returns: all known sitemap URLs
pub fn fetch_sitemaps(domain: &str) -> Result<Vec<String>, Box<dyn std::error::Error>> {
    let mut ret = Vec::new();
    let mut domain = domain.to_string();

    if domain.ends_with('/') {
        domain.pop().unwrap();
    }

    let sitemap_url = [&domain, "/sitemap.xml"].concat();

    let response = ureq::get(&sitemap_url).call()?;
    if response.status() == 200 {
        ret.push(sitemap_url);
    }

    let robots = Robots::new(&net::fetch_content(&[&domain, "/robots.txt"].concat()));
    ret.extend(robots.sitemaps);

    return Ok(ret);
}

/// usage: parse_sitemap_recursive("https://example.com")
pub fn parse_sitemap_recursive(xml: &str) -> Result<Vec<String>, Box<dyn std::error::Error>> {
    let html = Html::parse_document(xml);
    let sitemap_selector = Selector::parse("sitemap").unwrap();

    let sitemaps = html.select(&sitemap_selector);

    let mut sitemap_urls: Vec<String> = Vec::new();

    let loc_siblings = Selector::parse("* + loc").unwrap();

    for sitemap in sitemaps {
        let urls: Vec<_> = sitemap.select(&loc_siblings).collect();

        if urls.len() != 1 {
            log::error!(
                "multiple <loc> tags found in sitemap (SHA256 of sitemap is {:x})",
                crypto::sha256(xml)
            );
        }
        for url in &urls {
            let text = url.text().collect();
            sitemap_urls.push(text);
        }
    }

    let loc = Selector::parse("loc").unwrap();
    let mut locs: Vec<String> = html
        .select(&loc)
        .map(|loc| loc.text().map(|loc| loc.trim().to_string()).collect())
        .collect();

    for sitemap_url in sitemap_urls {
        log::debug!("SITEMAP URL: {}", sitemap_url);
        let xml = net::fetch_content(&sitemap_url);
        let additional_locs = parse_sitemap_recursive(&xml)?;
        locs.extend(additional_locs);
    }

    {
        let mut f = File::create("/tmp/website-sitemap.log")?;
        for loc in &locs {
            f.write_all(loc.as_bytes())?;
        }
    }

    return Ok(locs);
}
