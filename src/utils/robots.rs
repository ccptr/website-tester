pub struct Robots {
    pub data: Vec<(String, String)>,
    pub sitemaps: Vec<String>,
}

impl Robots {
    pub fn new(content: &str) -> Self {
        let mut data: Vec<(String, String)> = Vec::new();
        let mut sitemaps = Vec::new();

        for line in content.lines() {
            let (key, value) = line.split_once(':').expect("received invalid content");

            let key = key.trim().to_string();
            let value = value.trim().to_string();

            if key.eq_ignore_ascii_case("sitemap") {
                sitemaps.push(value.clone());
            }

            data.push((key, value));
        }

        Self { data, sitemaps }
    }
}
