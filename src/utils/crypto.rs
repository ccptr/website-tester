use sha2::{Digest, Sha256};

pub fn sha256(data: impl AsRef<[u8]>) -> sha2::digest::Output<Sha256> {
    let mut xml_hash = Sha256::new();

    xml_hash.update(data);
    xml_hash.finalize()
}
