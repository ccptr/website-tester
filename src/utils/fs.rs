use std::path::{Path, PathBuf};

pub fn get_files<P>(path: P) -> Result<Vec<PathBuf>, std::io::Error>
where
    P: AsRef<Path>
{
    let mut ret = Vec::new();

    for entry in std::fs::read_dir(path)? {
        let path = entry?.path();
        if path.is_file() {
            ret.push(path);
        }
    }

    Ok(ret)
}
