// https://stackoverflow.com/a/33206814

#[allow(dead_code)]
pub mod effects {
    /// all attributes off
    pub const RESET: &str = "0";
    pub const BOLD : &str = "1";
    /// Not widely supported.
    pub const FAINT: &str = "2";
    /// Not widely supported. Sometimes treated as inverse.
    pub const ITALIC: &str = "3";
    pub const UNDERLINE: &str = "4";
    /// less than 150 per minute
    pub const SLOW_BLINK: &str = "5";
    /// MS-DOS ANSI.SYS; 150+ per minute; not widely supported
    pub const RAPID_BLINK: &str = "6";
    /// swap foreground and background colors
    pub const REVERSE_VIDEO: &str = "7";
    /// Not widely supported.
    pub const CONCEAL: &str = "8";
    /// Characters legible, but marked for deletion. Not widely supported.
    pub const CROSSED_OUT: &str = "9";
    /// default font
    pub const PRIMARY_FONT: &str = "10";
    /// Select alternate font 1
    pub const ALTERNATE_FONT_1: &str = "11";
    /// Select alternate font 2
    pub const ALTERNATE_FONT_2: &str = "12";
    /// Select alternate font 3
    pub const ALTERNATE_FONT_3: &str = "13";
    /// Select alternate font 4
    pub const ALTERNATE_FONT_4: &str = "14";
    /// Select alternate font 5
    pub const ALTERNATE_FONT_5: &str = "15";
    /// Select alternate font 6
    pub const ALTERNATE_FONT_6: &str = "16";
    /// Select alternate font 7
    pub const ALTERNATE_FONT_7: &str = "17";
    /// Select alternate font 8
    pub const ALTERNATE_FONT_8: &str = "18";
    /// Select alternate font 9
    pub const ALTERNATE_FONT_9: &str = "19";
    /// hardly ever supported
    pub const FRAKTUR: &str = "20";
    /// Bold off not widely supported; double underline hardly ever supported.
    pub const RESET_BOLD_AND_DOUBLE_UNDERLINE: &str = "21";
    /// Neither bold nor faint
    pub const NORMAL_INTENSITY: &str = "22";
    pub const RESET_ITALIC_OR_FRAKTUR: &str = "23"; // TODO: better name
    /// Not singly or doubly underlined
    pub const RESET_UNDERLINE: &str = "24";
    pub const RESET_BLINK: &str = "25";
    pub const RESET_INVERSE: &str = "27";
    /// CONCEAL off
    pub const REVEAL: &str = "28";
    pub const RESET_CROSSED_OUT: &str = "29";
    pub const FRAMED: &str = "51";
    pub const ENCIRCLED: &str = "52";
    pub const OVERLINED: &str = "53";
    pub const RESET_FRAMED_OR_ENCIRCLED: &str = "54";
    pub const RESET_OVERLINED: &str = "55";
    /// hardly ever supported
    pub const IDEOGRAM_UNDERLINE: &str = "60";
    /// hardly ever supported
    pub const IDEOGRAM_DOUBLE_UNDERLINE: &str = "61";
    /// hardly ever supported
    pub const IDEOGRAM_OVERLINE: &str = "62";
    /// hardly ever supported
    pub const IDEOGRAM_DOUBLE_OVERLINE: &str = "63";
    /// hardly ever supported
    pub const IDEOGRAM_STRESS_MARKING: &str = "64";
    /// reset the effects of all of `IDEOGRAM_*`
    pub const RESET_IDEOGRAM: &str = "65";
}

#[allow(dead_code)]
pub mod bg {
    pub const BLACK: &str = "40";
    pub const RED: &str = "41";
    pub const GREEN: &str = "42";
    pub const YELLOW: &str = "43";
    pub const BLUE: &str = "44";
    pub const MAGENTA: &str = "45";
    pub const CYAN: &str = "46";
    pub const WHITE: &str = "47";
    // pub const : &str = "48";
    // pub const : &str = "49";

    /// aixterm (not in standard)
    pub const BRIGHT_BLACK: &str = "100";
    /// aixterm (not in standard)
    pub const BRIGHT_RED: &str = "101";
    /// aixterm (not in standard)
    pub const BRIGHT_GREEN: &str = "102";
    /// aixterm (not in standard)
    pub const BRIGHT_YELLOW: &str = "103";
    /// aixterm (not in standard)
    pub const BRIGHT_BLUE: &str = "104";
    /// aixterm (not in standard)
    pub const BRIGHT_MAGENTA: &str = "105";
    /// aixterm (not in standard)
    pub const BRIGHT_CYAN: &str = "106";
    /// aixterm (not in standard)
    pub const BRIGHT_WHITE: &str = "107";
}

#[allow(dead_code)]
pub mod fg {
    pub const BLACK: &str = "30";
    pub const RED: &str = "31";
    pub const GREEN: &str = "32";
    pub const YELLOW: &str = "33";
    pub const BLUE: &str = "34";
    pub const MAGENTA: &str = "35";
    pub const CYAN: &str = "36";
    pub const WHITE: &str = "37";
    // const : &str = "38";
    // const : &str = "39";

    /// aixterm (not in standard)
    pub const BRIGHT_BLACK: &str = "90";
    /// aixterm (not in standard)
    pub const BRIGHT_RED: &str = "91";
    /// aixterm (not in standard)
    pub const BRIGHT_GREEN: &str = "92";
    /// aixterm (not in standard)
    pub const BRIGHT_YELLOW: &str = "93";
    /// aixterm (not in standard)
    pub const BRIGHT_BLUE: &str = "94";
    /// aixterm (not in standard)
    pub const BRIGHT_MAGENTA: &str = "95";
    /// aixterm (not in standard)
    pub const BRIGHT_CYAN: &str = "96";
    /// aixterm (not in standard)
    pub const BRIGHT_WHITE: &str = "97";
}
