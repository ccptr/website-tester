mod args;
mod tests;
mod utils;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();

    let args = args::Args::parse();

    match args.command {
        args::Command::RunAll   { sitemap_urls } => tests::run_all  ::run(&sitemap_urls),
        args::Command::TestUrls { sitemap_urls } => tests::test_urls::run(&sitemap_urls),
        args::Command::Tmp => tests::tmp::run(&args),
    }
}
